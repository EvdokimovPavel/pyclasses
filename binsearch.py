def binary_search (elem, *array):
    if len(array) == 0:
        print("Array is empty!")
        return 0
    
    left = 0
    if elem <= array[left]:
        return 0
    
    right = len(array) - 1
    if elem >= array[right]:
        return len(array)
    
    while (right - left) > 1:
        midl = (right+left)//2
        if elem == array[midl]:
            return midl
        if elem > array[midl]:
            left = midl
        else: right = midl
    
    return right