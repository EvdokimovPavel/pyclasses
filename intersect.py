def intersect(*arrays):
    result = set(arrays[0])
    
    for i in range(1,len(arrays)):
        x=set(arrays[i])
        res_remove = set()
        
        for check in result:
            if check not in x:
                res_remove.add(check)
                
        for i in res_remove:
            result.remove(i)
            
    return result