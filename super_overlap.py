def super_overlap(n, m, *sequences):
    l = len(sequences)
    summary = dict()
    
    for arr in range(0, l):
        for item in sequences[arr]:
            if item in summary:
                summary[item][arr] += 1
            else:
                summary[item] = []
                for x in range(0,l):
                    summary[item].append(0)
                summary[item][arr] += 1
    
    print(summary)
    result = set()
    
    for check in summary:
        num_of_seq = 0
        for iterator in range(0,l):
            if summary[check][iterator] >= m:
                num_of_seq += 1
        if num_of_seq >= n:
            result.add(check)
            
    return result